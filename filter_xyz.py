#!/usr/bin/python3
import sys
import math

def read_xyz_file(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()
        num_atoms = int(lines[0])
        comment = lines[1].strip()
        atoms = []
        for line in lines[2:]:
            atom_info = line.split()
            symbol = atom_info[0]
            x, y, z = map(float, atom_info[1:4])
            atoms.append((symbol, x, y, z))
    return num_atoms, comment, atoms

def distance(atom1, atom2):
    return math.sqrt((atom1[1] - atom2[1])**2 + (atom1[2] - atom2[2])**2 + (atom1[3] - atom2[3])**2)

def filter_atoms(atoms, reference_atoms, distance_criteria, num_closest):
    filtered_atoms = []
    deleted_atoms = set()
    for i, atom in enumerate(atoms):
        too_close = False
        for ref_atom in reference_atoms:
            if distance(atom, ref_atom) < distance_criteria:
                too_close = True
                break
        if not too_close:
            filtered_atoms.append(atom)
        else:
            deleted_atoms.add(i)  # Add the index of the deleted atom

    # Remove the specified number of closest neighbors for each deleted atom
    for i in deleted_atoms:
        distances = []
        for j, atom in enumerate(atoms):
            if i != j:  # Exclude the deleted atom itself
                distances.append((j, distance(atoms[i], atom)))

        # Sort distances by distance to the deleted atom
        distances.sort(key=lambda x: x[1])
        
        # Remove the specified number of closest atoms to the deleted atom
        for neighbor in distances[:num_closest]:
            if neighbor[0] not in deleted_atoms:
                filtered_atoms.append(atoms[neighbor[0]])

    return filtered_atoms

# File paths and distance criteria from command-line arguments
if len(sys.argv) < 5:
    print("Usage: python script.py first_structure.xyz second_structure.xyz distance_criteria num_closest")
    sys.exit(1)

first_structure_path = sys.argv[1]
second_structure_path = sys.argv[2]
distance_criteria = float(sys.argv[3])
num_closest = int(sys.argv[4])

# Read the structures
num_atoms_1, comment_1, atoms_1 = read_xyz_file(first_structure_path)
num_atoms_2, comment_2, atoms_2 = read_xyz_file(second_structure_path)

# Filter atoms in the second structure based on distance criteria from the first structure
filtered_atoms_2 = filter_atoms(atoms_2, atoms_1, distance_criteria, num_closest)

# Write the filtered structure to stdout
print(f"{len(filtered_atoms_2)}")
print(comment_2)
for atom in filtered_atoms_2:
    print(f"{atom[0]} {atom[1]} {atom[2]} {atom[3]}")

