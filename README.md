# XYZ Structure Filtering

This Python script filters atoms from a second XYZ structure file based on a chosen distance criteria from a first XYZ structure file. It provides an option to remove a specified number of closest neighboring atoms when an atom is filtered out.

## Usage

### Prerequisites

- Python 3.x

### Installation

1. Clone or download this repository.

### Running the Script

Run the script using the command line as follows:

```bash
python filter_xyz.py first_structure.xyz second_structure.xyz distance_criteria num_closest
```

Replace `first_structure.xyz`, `second_structure.xyz`, `distance_criteria`, and `num_closest` with the actual paths of your XYZ files and the desired distance criteria value, respectively.

- `first_structure.xyz`: Path to the XYZ file containing the reference structure (format: number of atoms, comment, element symbol, x-coordinate, y-coordinate, z-coordinate for each atom).
- `second_structure.xyz`: Path to the XYZ file containing the structure to be filtered.
- `distance_criteria`: The distance criteria used to filter atoms from the second structure. Only atoms in the second structure that are farther than this distance from any atom in the first structure will be retained.
- `num_closest`: The number of closest neighboring atoms to delete when an atom is filtered out due to proximity.

The filtered output will be displayed on the terminal and can be redirected to a file.

### Notes
- Ensure that the XYZ files follow the specified format: number of atoms, comment line, and atom information lines.
- The output will be displayed on the terminal; you can redirect it to a file using standard command-line operators if needed (>, >>).
